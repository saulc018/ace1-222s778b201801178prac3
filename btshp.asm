include include/IO.asm

.MODEL SMALL
.STACK
;# DATA SEGMENT
.DATA
dev_info db "Universidad de San Carlos de Guatemala",0ah,
            "Facultad de Ingenieria",0ah,
            "Escuela de Ciencias y Sistemas", 0ah,
            "Arquitectura de Computadoras  y Ensambladores 1",0ah,
            "Seccion B",0ah,
            "Saul Castellanos",0ah,
            "201801178",0ah,"$"
menu_opt db "1) Iniciar Juego",0ah,
            "2) Cargar Juego",0ah,
            "3) Salir",0ah,0ah,
            "Escoja una opcion[1-3]: ","$"
rows_header db "123456789A ", "$"
cols_header db "JIHGFEDCBA", "$"
divider db "-+-+-+-+-+-+-+-+-+-","$"
wait_msg db "Presiona <ENTER> para continuar...","$"
ships_size db 02h,03h,03h,04h,05h  ;Espacios que ocupa cada tipo de barco
;## ESTADO DE JUEGO
P1_upper_board db 100 dup("#"), "$"
P1_lower_board db 100 dup("#"), "$"
P2_upper_board db 100 dup("#"), "$"
P2_lower_board db 100 dup("#"), "$"
P1_ships_IDs db 5 dup(0), "$"   ;tipos de barco
P2_ships_IDs db 5 dup(0), "$"
P1_ships_pos db 5 dup(0), "$"   ;fila o col donde esta cada barco
P2_ships_pos db 5 dup(0), "$"
P1_ships_rot db 5 dup(0), "$"   ;orientacion de cada barco
P2_ships_rot db 5 dup(0), "$"
;## PROPOSITO GENERAL
row dw 00h
col dw 00h
;# CODE SEGMENT
.CODE
main:
    mov AX, @DATA
    mov DS, AX
show_info:
    MC_cls
    MC_printnl dev_info
    MC_wait_confirm
show_menu:
    MC_cls
    MC_print menu_opt
    MC_getKey
    cmp AL, "1"
    je start_game
    cmp AL, "2"
    je load_game
    cmp AL, "3"
    je exit
    jmp show_menu
start_game:
    MC_cls
    MC_print_board P1_lower_board
    MC_wait_confirm
    jmp show_menu
load_game:
    jmp show_menu
exit:
    MC_cls
    .EXIT 0 ;fin del programa
END main