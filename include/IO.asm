include board.asm
;MACROS PARA INPUT Y OUTPUT

;Imprime una cadena en pantalla e inserta un salto de linea
;@param str - la cadena a imprimir
MC_printnl macro str
    mov AH, 09h
    mov DX, OFFSET str
    int 21h
    MC_print_char 0ah
    endm
;Imprime una cadena en pantalla
;@param str - la cadena a imprimir
MC_print macro str
    mov AH, 09h
    mov DX, OFFSET str
    int 21h
    endm
;espera a que el usuario presione una tecla e inserta un salto de linea
MC_print_char macro char
    mov AH, 02h
    mov DL, char
    int 21h
    endm
MC_wait_confirm macro
    MC_print wait_msg
    mov AH, 0ah
    int 21h
    endm
;lee un caracter del standard input
MC_getKey macro
    mov AH,01h
    int 21h
    endm
;limpia la consola
MC_cls macro
    mov AX, 03h
    int 10h
    endm
;muestra un tablero en pantalla
MC_print_board macro board
LOCAL for_rows, for_cols, new_line, col_header, break
    push BX
for_rows:
    ;MC_printnl divider
    mov BX, row
    MC_print_char rows_header[BX]
    MC_print_char 20h
    cmp row, 0ah
    je col_header        ;romper loop si row=10
for_cols:
    MC_get_index row, col
    MC_print_char board[BX]
    cmp col, 09h
    je new_line     ;romper loop si col=9
    inc col
    MC_print_char 20h
    jmp for_cols
new_line:
    inc row
    mov col, 00h
    MC_print_char 0ah
    jmp for_rows
col_header:
    mov BX, col
    MC_print_char cols_header[BX]
    cmp col, 09h
    je break
    inc col
    MC_print_char 20h
    jmp col_header
break:
    mov col, 00h
    mov row, 00h
    pop BX
    endm