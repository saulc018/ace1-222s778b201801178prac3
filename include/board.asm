;MACROS PARA MANEJAR TABLEROS

;almacena dentro del registro SI el indice
;correspondiente a una matriz 10x10 por medio de row major
;indice = row*10+col
MC_get_index macro row, col
    mov AX, row
    mov BX, 0Ah
    mul BX
    add AX, col
    mov BX, AX
    endm